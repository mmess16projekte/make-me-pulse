# How to run MakeMePulse-App

You can either access the online version via [MakeMePulse](http://imbücherturm.de/mmp/index.html) or create a local copy of the Bitbucket repository [MakeMePulse@Bitbucket](https://bitbucket.org/mmess16projekte/make-me-pulse)

##Requirements
To view the Website, you need an uptodate Browser. Preferably one of the following:
- Chrome (Version 53.0 +)
- Firefox (Version 47.0 +)
- Opera (Version 39.0)

## No need to install something

There is no node.js server or something else.
If you have local version of MakeMePulse (e.i. the Bitbucket repository), just open the index.html in your browser.
