var App = App || {};
App.WodListController = function() {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = new EventPublisher();

  function setEvents() {
    //weight submission field for collecting user weight data.
    $("#submitWeightForm button").click(function(event) {
      var weight = $("#submitWeightForm input").val();
      that.notifyAll("onWeightSubmitClicked", weight);
    });
    //Filter Events  
    //search input for free text
    var searchInput = document.getElementById("wodSearch");
    searchInput.addEventListener("input", function(event) {
      that.notifyAll("onSearchQueryInput", event.target.value);
    });
    //AMRAP checkbox
    var chkAMRAP = document.getElementById("chkAMRAP");
    chkAMRAP.addEventListener("click", function(event) {
      that.notifyAll("onSelectTyp", event.target);
    });
    //FT checkbox
    var chkFT = document.getElementById("chkFT");
    chkFT.addEventListener("click", function(event) {
      that.notifyAll("onSelectTyp", event.target);
    });
    //exercises selection
    $( ".chk-exercise" ).click(function(event) {
      that.notifyAll("onSelectExercise", event.target);
    });
    //equipment selection
    $( ".chk-equip" ).click(function(event) {
      that.notifyAll("onSelectEquipment", event.target);
    });
    //Wod Click Event
    var listEntry = document.getElementById("wod-list");
    listEntry.addEventListener("click", function(event) {
      that.notifyAll("onListEntryClicked", getParentWithClass(event.target, "wod_list_entry").getAttribute("wod-id"));
    });
  }

  function getParentWithClass(element, targetClass) {
    if (element.classList.contains(targetClass)) {
      return element;
    } else if (element.parentElement !== null){
      return getParentWithClass(element.parentElement, targetClass);
    }
    return null;
  }

  $('#submitWeightForm').submit(function(e) {
        e.preventDefault();
    });

  function init() {
    setEvents();
  }

  that.init = init;
  return that;
};