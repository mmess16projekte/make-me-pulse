var App = App || {};
App.mmp = (function() {
  "use strict";
  /* eslint-env browser  */

  var that = {},
    viewController,
    welcomeController,
    wodListModel,
    wodListView,
    wodListController,
    wodExecuteView,
    wodExecuteController,
    wodExecuteModel,
    statsModel,
    statsView,
    statsController,
    wodDetailView,
    pouchDbModule;

  function initViewController() {
    viewController = App.viewController();
    viewController.init();
    viewController.addEventListener("onNavWodListClicked", onNavWodListClicked);
    viewController.addEventListener("onNavStatsClicked", onNavStatsClicked);
  }

  function onNavWodListClicked() {
    viewController.showView("list");
  }

  /**
   * if the user clicks on Statistics, the statsModel gets filled with the data it needs to function:
   * user object from db, the stats model already holds a array of existing all wods. with those two an
   * array of all wods the user has completed gets generated. this array is passed down to statsView where it
   * will be renderd as a list at the side of the view.
   * all functioncalls are within the promise call because they all depend on each other and have to be executed in this order.
   * @return {none}
   */
  function onNavStatsClicked() {
    var statsList,
      bwiString,
      weightList;
    pouchDbModule.getUser().then(function(result) {
      statsModel.setUser(result);
      statsList = statsModel.buildUserWodList();
      weightList = statsModel.buildWeightDataTableArray();
      if (statsList[0] !== undefined) {
        statsView.updateChart(statsModel.buildDataTableArray(statsList[0].id));
      }
      statsView.updateWeightChart(weightList);
      statsView.showStatsList(statsList);
      statsView.displayBwiString(statsModel.buildBwiString());
      viewController.showView("stats");
    }).catch(function(err) {
      console.log("user not set in statsModel! ", err);
    });
  }

  function initWodExecute() {
    initWodExecuteView();
    initWodExecuteController();
    initWodExecuteModel();
  }

  function initWodExecuteView() {
    wodExecuteView = new App.wodExecuteView();
    wodExecuteView.init();
  }

  function initWodExecuteController() {
    wodExecuteController = new App.wodExecuteController();
    wodExecuteController.init();
    wodExecuteController.addEventListener("onSubmitReps", onSubmitReps);
    wodExecuteController.addEventListener("onSubmitTime", onSubmitTime);
    wodExecuteController.addEventListener("onToggleTimer", onToggleTimer);
  }

  /**
   * user gets updated after score is submitter but only with the help of the "magicnumber" lonelyuser. this is neccessary since we don't have a 
   * central user administration which holds the current user of the session.
   * @param  {int} score
   * @return {none}
   */
  function onSubmitReps(score) {
    pouchDbModule.getUser().then(function(result) {
      var user = wodExecuteModel.addRepsToUser(result, score.data);
      pouchDbModule.updateUser(user);
    }).catch(function(err) {
      console.log("user not retrived and not updated");
    });
  }

  function onSubmitTime() {
    pouchDbModule.getUser().then(function(result) {
      var user = wodExecuteModel.addTimeToUser(result);
      pouchDbModule.updateUser(user);
    }).catch(function(err) {
      console.log("user not retrived and not updated");
    });
  }

  function onToggleTimer() {
    wodExecuteModel.toggleTimer();
  }

  function initWodExecuteModel() {
    wodExecuteModel = App.wodExecuteModel();
    wodExecuteModel.addEventListener("setView", setExecuteView);
    wodExecuteModel.addEventListener("setTimerTime", setTimerTime);
    wodExecuteModel.addEventListener("onScoreSubmited", onScoreSubmited);
    wodExecuteModel.addEventListener("toogleTimerButton", toogleTimerButton);
    wodExecuteModel.addEventListener("showRepsSubmitButton", showRepsSubmitButton);
    wodExecuteModel.addEventListener("disableTimeSubmitButton", disableTimeSubmitButton);
  }

  function setExecuteView(wod) {
    wodExecuteView.setCurrentWod(wod.data);
  }

  function setTimerTime(time) {
    wodExecuteView.setTimerTime(time.data);
  }

  function onScoreSubmited(submited) {
    if (submited.data) {
      wodExecuteView.showSuccessAlertExecute();
    } else {
      wodExecuteView.showWarningAlertExecute();
    }
  }

  function toogleTimerButton(mode) {
    wodExecuteView.toggleTimerButton(mode.data);
  }

  function showRepsSubmitButton(){
    wodExecuteView.showRepsSubmitButton();
  }

  function disableTimeSubmitButton(disable){
    wodExecuteView.disableTimeSubmitButton(disable.data);
  }

  function initPouchDbModule() {
    pouchDbModule = App.PouchDbModule();
    $("#deleteDbButton").click(function() {
      var db = new PouchDB('MMP');
      db.destroy().then(function(response) {location.reload();}).catch(function(err) {
        consoloe.log(err);
      });
    });
  }

  function initWelcome() {
    welcomeController = App.WelcomeController();
    welcomeController.addEventListener("onUserDataSubmited", onUserDataSubmited);
  }

  function onUserDataSubmited(user) {
    var user = user.data;
    if (welcomeController.checkForm()) {
      pouchDbModule.addUser(user);
      viewController.showView("list");
    };
  }

  /**
   * after user clicks the start button the exercise div will be cleared from remaining data and a new html segment with new data will be attached
   * @return {none}
   */
  function initWodDetail() {
    wodDetailView = App.wodDetailView().init();
    document.getElementById("wodDetailStartWod").addEventListener("click", function(event) {
      wodExecuteModel.setCurrentWod(wodListModel.getWod(event.target.getAttribute("wod-id")));
      viewController.showView("execute");
    });
  }

  function initWodList() {
    initWodListModel();
    initWodListView();
    initWodListController();
  }

  function initWodListModel() {
    wodListModel = new App.WodListModel({
      wodList: JSON.parse(document.querySelector("#wodsJSON").innerHTML)
    });
    wodListModel.init();
  }

  function initWodListView() {
    wodListView = new App.WodListView({
      wodListElement: document.querySelector("#wod-list"),
      wodTemplate: $("#wod-list-entry-template").html(),
      wodList: wodListModel.getWodList()
    });
    wodListView.init();
  }

  function initWodListController() {
    wodListController = new App.WodListController();
    wodListController.init();
    wodListController.addEventListener("onWeightSubmitClicked", onWeightSubmitClicked);
    wodListController.addEventListener("onListEntryClicked", openDetails);
    //Filter Events
    wodListController.addEventListener("onSearchQueryInput", filterName);
    wodListController.addEventListener("onSelectTyp", filterTyp);
    wodListController.addEventListener("onSelectExercise", filterExercise);
    wodListController.addEventListener("onSelectEquipment", filterEquipment);
  }

  function onWeightSubmitClicked(weight) {
    // update user in db
    var weight = weight.data.toString();
    weight.replace(",", ".");
    if (weight !== "") {
      pouchDbModule.getUser().then(function(result) {
        var date = Date.now();
        result.weight.push({
          weight: weight,
          timestamp: date
        });
        pouchDbModule.updateUser(result);
        wodListView.showSuccessAlert(weight);
        wodListView.emptyInputfield();
      }).catch(function(err) {
        console.log("user weight not updated", err);
      });
    };
  }

  function openDetails(wodID) {
    var wod = wodListModel.getWod(wodID.data);
    wodDetailView.activateModal(wod);
  }

  function filterName(query) {
    wodListView.showWods(wodListModel.getFilteredWodList("name", query.data));
  }

  function filterTyp(target) {
    wodListView.showWods(wodListModel.getFilteredWodList(target.data.id.substr(3, target.data.id.length), target.data.checked));
  }

  function filterExercise(target) {
    wodListView.showWods(wodListModel.getFilteredWodList("exercise", target.data.value, target.data.checked));
  }

  function filterEquipment(target) {
    wodListView.showWods(wodListModel.getFilteredWodList("equip", target.data.value, target.data.checked));
  }

  function initStats() {
    initStatsModel();
    initStatsView();
    initStatsController();
  }

  function initStatsModel() {
    var wodList = wodListModel.getWodList();
    statsModel = new App.statsModel({
      wodList: wodList
    });
    statsModel.init();
  }

  function initStatsView() {
    statsView = new App.statsView();
    statsView.init();
  }

  function initStatsController() {
    statsController = new App.statsController();
    statsController.init();
    statsController.addEventListener("statsListEntryClicked", onStatsListEntryClicked);
  }

  function onStatsListEntryClicked(wodId) {
    var table = statsModel.buildDataTableArray(wodId.data);
    statsView.updateChart(table);
  }

  /**
   * tests if user is in db and changes displayed content according to result
   * should be in ViewController
   * @return {none}
   */
  function toggleView() {
    pouchDbModule.isUserInDb().then(function(result) {
      if (result) {
        viewController.showView("list");
      } else {
        viewController.showView("login");
      }
    }, function(err) {
      console.log("failed but still a mastahpiece", err);
    });
  }

  function init() {
    initViewController();
    initWodExecute();
    initPouchDbModule();
    initWelcome();
    initWodDetail();
    initWodList();
    initStats();

    toggleView();

    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip(); 
    });
  }

  that.init = init;
  return that;
}());