var App = App || {};
App.statsView = function(options) {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = {},
    statsList,
    weightList,
    chart,
    weightChart,
    diagrammWidth = "700",
    weightDiagramWidth = "1000",
    statsTemplate;

  /**
   * expects an array of wods sortert by the order the user completet them last.
   * then renders those wods in a list at the side of the stats view
   * @param  {arry} statslist
   * @return {none}
   */
  function showStatsList(statslist) {
    var statsListElement = document.querySelector("#stats-list");
    statsListElement.innerHTML = "";
    var i;
    var entryNode = document.createElement("div");
    for (i = 0; i < statslist.length; i++) {
      entryNode.innerHTML = statsTemplate(statslist[i]);
      statsListElement.appendChild(entryNode.children[0]);
    }
  }

  function createTamplate() {
    statsTemplate = _.template($("#stats-list-entry-template").html());
  }

  function drawWeightChart() {
    var data = google.visualization.arrayToDataTable([
      ['Date', 'Weight'],
       ['01.04.2016', 20]
    ]);

    var options = {
      title: "Weight Overview",
      width: weightDiagramWidth,
      legend: {
        position: "bottom"
      },
      backgroundColor: {
        fill: 'transparent'
      }
    };
    weightChart = new google.visualization.LineChart(document.getElementById('weightChart'));
    weightChart.draw(data, options);
  }

  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Date', 'Reps'],
      ["11.11.2016",0]
    ]);

    var options = {
      title: 'nothing here yet',
      width: diagrammWidth,
      legend: {
        position: 'bottom'
      },
      backgroundColor: {
        fill: 'transparent'
      }
    };
    chart = new google.visualization.LineChart(document.getElementById('statsChart'));
    chart.draw(data, options);
  }

  /**
   * updates the scores chart of the selected wod. Also sets the name of the selected wod in the chart.
   * @param  {object} wodNameAndArray is an object with 2 properties: name, the wod name as string. dataTableArray, the array returned from buildDataTableArray in WodStatsModel.
   * @return {none}
   */
  function updateChart(wodNameAndArray) {
    var data = google.visualization.arrayToDataTable(wodNameAndArray.dataTableArray);
    var options = {
      title: wodNameAndArray.name,
      width: diagrammWidth,
      legend: {
        position: 'bottom'
      },
      backgroundColor: {
        fill: 'transparent'
      }
    };
    chart.draw(data, options);
  }

  function updateWeightChart(weightArray) {
    var data = google.visualization.arrayToDataTable(weightArray);
    var options = {
      title: "Weight Overview",
      width: weightDiagramWidth,
      legend: {
        position: "bottom"
      },
      backgroundColor: {
        fill: 'transparent'
      }
    };
    weightChart.draw(data, options);
  }

  function displayBwiString(bwiString) {
    $("#bwiStringParagraph").html(bwiString);
  }

  function init() {
    google.charts.load('current', {
      'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);
    google.charts.setOnLoadCallback(drawWeightChart);
    createTamplate();
  }

  that.init = init;
  that.displayBwiString = displayBwiString;
  that.updateWeightChart = updateWeightChart;
  that.updateChart = updateChart;
  that.showStatsList = showStatsList;
  return that;
};