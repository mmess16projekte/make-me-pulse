var App = App || {};
App.PouchDbModule = function() {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = new EventPublisher(),
    db = new PouchDB('MMP'),
    remoteCouch = false,
    userId = "lonelyUser";

  /**
   * inserts user into db but as an object not as json
   * @param {userObject} user
   */
  function addUser(user) {
    user._id = userId;
    db.put(user, function callback(err, result) {
      if (err) {}
    });
  }

  /**
   * looks for users in db and sends result to mmp.js for controling which html to display
   * @return {none}
   */
  function getUsers() {
    var users = db.query(getUsersMap).then(function(result) {
      that.notifyAll('onGetUsers', result);
    }).catch(function(err) {
      console.log(err);
    });
  }

  /**
   * map function for getting all users from db
   * @param  {[type]} doc
   * @return {none}
   */
  function getUsersMap(doc) {
    if (doc.type === "user") {
      emit(doc._id);
    }
  }

  /**
   * test if user is already in db and returns a boolean. this function is used to controll if the welcomeView
   * should be shown or not. 
   * @return {Boolean}
   */
  function isUserInDb() {
    return new Promise(function(resolve, reject) {
      db.query(getUsersMap).then(function(result){
        var userPresent = false;
        if(result.total_rows > 0) {
          userPresent = true;
        } 
        resolve(userPresent);
      }).catch(function(err) {
        reject(err);
      });
    });
  }

  /**
   * getUser returns the user object after it is retrived. A promise is used
   * to only return data after the user object is retrived.
   * @param  {int} userId
   * @return {userobject}
   */
  function getUser() {
    return new Promise(function(resolve, reject) {
      db.get(userId).then(function(doc) {
        resolve(doc);
      }).catch(function(err) {
        reject(err);
      });
    });
  }

  /**
   * updates the user in db. is used if a new score or weight was added to the user object
   * @param  {object} user
   * @return {none}
   */
  function updateUser(user) {
    db.put(user).then(function(response) {}).catch(function(err) {
      console.log("wasnt successfully updating!");
    });
  }

  that.isUserInDb = isUserInDb;
  that.updateUser = updateUser;
  that.getUser = getUser;
  that.getUsers = getUsers;
  that.addUser = addUser;
  return that;
};