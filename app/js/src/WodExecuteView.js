var App = App || {};
App.wodExecuteView = function() {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = {},
  templateExerciseView,
  createTemplateExerciseView,
  exerciseDiv,
  wod,
  btnTimer = document.getElementById("toggle-timer"),
  timerTime = document.getElementById("timer");

  function setCurrentWod(wod) {
    this.wod = wod;
    clear();
    renderWod(wod);
  }

  function init() {
    exerciseDiv = document.querySelector("#genaueInfos");
  }

  function renderWod (wod) {
    clear();
    createDetailTemplate();
    addListEntry(wod);
    timerTime.innerHTML = wod.timeReadable;
    if (wod.type === "ft") {
      $("#submitRepsForm").hide();
      $("#submitTimeForm").show();
    }
    else {
      $("#submitRepsForm").hide();
      $("#submitTimeForm").hide();
    }
  }

   function createDetailTemplate() {
    templateExerciseView = document.querySelector("#wod-exercise-template").innerHTML;
    createTemplateExerciseView = _.template(templateExerciseView);
  }

  function addListEntry(wod) {
    var entryNode = document.createElement("div");
    entryNode.innerHTML = createTemplateExerciseView(wod);
    exerciseDiv.appendChild(entryNode.children[0]);
  }

  function clear() {
    while (exerciseDiv.firstChild) {
      exerciseDiv.removeChild(exerciseDiv.firstChild);
    }
    toggleTimerButton("Start");
  }

  function setTimerTime(time) {
    timerTime.innerHTML = time;
  }

  function toggleTimerButton(mode) {
    btnTimer.innerHTML = mode;
  }

  function showRepsSubmitButton(){
      $("#submitRepsForm").show();
  }

  function disableTimeSubmitButton(disabled){
    if (disabled) {
      $("#submitTimeForm button").addClass("disabled");
      $("#submitTimeForm button").attr("disabled", true);
    }
    else {
      $("#submitTimeForm button").removeClass("disabled");
      $("#submitTimeForm button").attr("disabled", false);
    }
  }

  function showSuccessAlertExecute() {
    $("#submitScoreAlert").addClass("alert-success");
    $("#submitScoreAlert").html("your <strong>Score</strong> was <strong>submitted!</strong>!");
    $("#submitScoreAlert").addClass("in");
    setTimeout(function() {
      $("#submitScoreAlert").removeClass("in");
      $("#submitScoreAlert").removeClass("alert-success");
    }, 3500);
  }

  function showWarningAlertExecute() {
    $("#submitScoreAlert").addClass("alert-warning");
    $("#submitScoreAlert").html("Scores under 15 seconds can't be submitted. Nobody is that fast ;)");
    $("#submitScoreAlert").addClass("in");
    setTimeout(function() {
      $("#submitScoreAlert").removeClass("in");
      $("#submitScoreAlert").removeClass("alert-warning");
    }, 3500);
  }

  that.init = init;
  that.showSuccessAlertExecute = showSuccessAlertExecute;
  that.showWarningAlertExecute = showWarningAlertExecute;
  that.setCurrentWod = setCurrentWod;
  that.setTimerTime = setTimerTime;
  that.toggleTimerButton = toggleTimerButton;
  that.showRepsSubmitButton = showRepsSubmitButton;
  that.disableTimeSubmitButton = disableTimeSubmitButton;
  return that;
};