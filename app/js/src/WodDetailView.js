var App = App || {};
App.wodDetailView = function() {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = new EventPublisher(),
    templateDetailView,
    createTemplateDetailView,
    detailModal,
    testButton;

  function init() {
    detailModal = document.querySelector(".modal-body");
    onModalHidden();
    return that;
  }

  function onModalHidden() {
    $("#wodDetail").on("hidden.bs.modal", function(e) {
      clearModal();
    });
  }

  /**
   * handles the templating and inserting workoutdata into the modal
   * @param  {Workout objec} workout
   * @return {none}
   */
  function fillModal(workout) {
    document.querySelector(".modal-title").innerHTML = workout.name;
    createDetailTemplate();
    addListEntry(workout);
    $("#wodDetailStartWod").attr("wod-id", workout.id);
  }

  /**
   * generates the template function for the modal
   * @return {function}
   */
  function createDetailTemplate() {
    templateDetailView = document.querySelector("#wod_detail_view_entry").innerHTML;
    createTemplateDetailView = _.template(templateDetailView);
  }

  /**
   * generates html from workout object and inserts it into the modal
   * @param {Workout object} workout
   */
  function addListEntry(workout) {
    var entryNode = document.createElement("div");
    entryNode.innerHTML = createTemplateDetailView(workout);
    detailModal.appendChild(entryNode.children[0]);
  }

  /**
   * emptys the modal for next use
   * @return {none}
   */
  function clearModal() {
    while (detailModal.firstChild) {
      detailModal.removeChild(detailModal.firstChild);
    }
  }

  /**
   * interface that awaits a workout object and handles the modal activation
   * @param  {Workout Object} workout
   * @return {none}
   */
  function activateModal(workout) {
    fillModal(workout);
    $("#wodDetail").modal();
  }

  that.init = init;
  that.activateModal = activateModal;
  return that;
};