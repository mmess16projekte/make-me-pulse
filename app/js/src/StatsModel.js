var App = App || {};
App.statsModel = function(options) {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = {},
    currentUser,
    fullWodList,
    userWodList;
  
  /**
   * builds an array of all the wods with scoresArray property the user has completed sortet by the timestap of the wods latest score
   * !!this function depends on the current user object and the full wod list!!
   * @return {array}
   */
  function buildUserWodList() {
    var scores,
    wodWtihScores,
    latestTimestamp,
    i;
    userWodList = [];
    scores = currentUser.done;
    for(i = 0; i<scores.length; i++) {
      if(scores[i] != undefined){
        wodWtihScores = fullWodList.find(function(wod) {
          return wod.id == i;
        })
        wodWtihScores.scores = scores[i];
        latestTimestamp = wodWtihScores.scores[wodWtihScores.scores.length - 1].timestamp;
        latestTimestamp = new Date(latestTimestamp);
        wodWtihScores.lastExercised = "zuletzt absolviert am " + latestTimestamp.toLocaleDateString() + " um " + latestTimestamp.toLocaleTimeString();
        userWodList.push(wodWtihScores);
      }
    }
    userWodList = userWodList.sort(compare);
    return userWodList;
  }

  /**
   * generates an object that holds the wodName and an array which is ready to use with google charts api. first the identifiers of the x and y axis are added to the array
   * after that within the for loop the scores with a formated timestamp get pushed into the arrray
   * !!this function only works after buildUserWodList() is called at least once. because it depends on the userWodList array!!
   * @param  {int} wodId
   * @return {object} ready to be used as dataTable
   */
  function buildDataTableArray(wodId) {
    var xAxis,
    yAxis,
    dataTableArray,
    selectedWod,
    result,
    i;
    xAxis = "Reps";
    yAxis = "Date";
    selectedWod = userWodList.find(function(wod) {
      return wod.id == wodId;
    });
    if (selectedWod.type === "ft") {
      xAxis = "Time in min";
    };
    dataTableArray = [[yAxis, xAxis]];
    for(i = 0; i < selectedWod.scores.length; i++) {
      var date = new Date(selectedWod.scores[i].timestamp);
      dataTableArray.push([date.toLocaleDateString(), parseFloat(parseFloat(selectedWod.scores[i].score).toFixed(2))]);
    };
    result = {
      name: selectedWod.name,
      dataTableArray: dataTableArray
    };
    return result;
  }

  /**
   * generates an array consisting of weight and time data. ready to use with the datatable class from google charts api.
   * @return {array}
   */
  function buildWeightDataTableArray() {
    var weightArray = [],
    userWeight,
    i;
    userWeight = currentUser.weight;
    weightArray[0] = ["Date", "Weight in Kg"];
    for(i = 0; i < userWeight.length; i++) {
      var date = new Date(userWeight[i].timestamp).toLocaleDateString();
      weightArray.push([date, parseInt(userWeight[i].weight)]);
    };
    return weightArray;
  }

  function compare(wodA, wodB) {
    var wodAScoreTime,
    wodBScoreTime;
    wodAScoreTime = wodA.scores[wodA.scores.length - 1].timestamp;
    wodBScoreTime = wodB.scores[wodB.scores.length - 1].timestamp;
    return wodBScoreTime - wodAScoreTime;
  }

  /**
   * creats a string to provide the user with info about his bodyweight.
   * @return {string}
   */
  function buildBwiString() {
    var bmiString,
    userWeight,
    bmi;
    userWeight = currentUser.weight[currentUser.weight.length-1].weight;
    userWeight = parseFloat(userWeight);
    bmi = (userWeight)/(currentUser.size/100);
    bmi = bmi.toFixed(2);
    bmiString = "Hello " + currentUser.name + " you have a BMI of " + bmi + " !";
    if (bmi > 25) {
      bmiString = bmiString + " your bodyweight is a little to high. You should train more! =)";
    }else if (bmi < 20) {
      bmiString = bmiString + " your bodyweight is a little to low. You should eat well after your Workout! =)";
    }else {
      bmiString = bmiString + " your bodyweight is healthy. Keep going! =)";
    };
    return bmiString;

  }

  function getWeightList() {
    return currentUser.weight;
  }

  function init() {
    fullWodList = options.wodList;
  }

  function setUser(user) {
    currentUser = user;
  }
 

  that.init = init;
  that.buildBwiString = buildBwiString;
  that.buildDataTableArray = buildDataTableArray;
  that.buildWeightDataTableArray = buildWeightDataTableArray;
  that.buildUserWodList = buildUserWodList;
  that.setUser = setUser;
  that.getWeightList = getWeightList;
  return that;
};