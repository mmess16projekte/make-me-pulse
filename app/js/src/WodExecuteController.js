var App = App || {};
App.wodExecuteController = function() {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = new EventPublisher(),
  scoreInputField = document.getElementById("Exercisescore"),
  btnSubmitReps = document.getElementById("submitReps"),
  btnSubmitTime = document.getElementById("submitTime"),
  btnTimer = document.getElementById("toggle-timer");

  function init() {
    btnSubmitReps.addEventListener("click", function () {
      that.notifyAll("onSubmitReps", scoreInputField.value);
    });

    btnSubmitTime.addEventListener("click", function () {
      that.notifyAll("onSubmitTime", null);
    });

    btnTimer.addEventListener("click", function () {
      that.notifyAll("onToggleTimer", null);
    });
  }

  $("#submitRepsForm").submit(function(e) {
    e.preventDefault();
  });

  that.init = init;
  return that;
};