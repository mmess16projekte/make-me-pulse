/**
 * @namespace wod_list_view
 * @memberOf! mmp
 * @description View zur Darstellung der filterbaren Workoutliste
 * <p>Die <code>wod_list_view</code> verwaltet die filterbare Workoutliste. </p>
 * <p>Options-Objekt:</p>
 * <ul>
 * </ul>
 */
var App = App || {};
App.WodListModel = function(options) {
  "use strict";
  /* eslint-env browser */

  var that = {},
    wodList,
    viewingWodList,
    filterList;

  function getWodList(){
    return wodList;
  }

  function getFilteredWodList(criteria, type, value) {
    addFilterValues(criteria, type, value);

    //Build RegEX for Workout Typ
    var strTypQuery = "(_|_)";
    if (filterList.amrap) {
      strTypQuery = "(amrap|_)";
    }
    if (filterList.ft) {
      strTypQuery = strTypQuery.substr(0, strTypQuery.indexOf('|')) + "|ft)";
    }
    var typRegEx = RegExp(strTypQuery, "i");
    viewingWodList = wodList.filter(function(wod) {

      if (checkComponents(wod.exercises, filterList.exercises, 0) &&checkComponents(wod.equipment, filterList.equipment, 2) ){
        if (wod.name.match(RegExp(filterList.name, "i")) !== null && wod.type.match(typRegEx) !== null) {
          return true;
        } 
      }
      return false;
    });
    return viewingWodList;
  }
  
  function getWod(wodId) {
    var wodListfilterd = wodList.filter(function(wod) {
      if (wodId == wod.id) {
        return true;
      } else {
        return false;
      }
    });
    return wodListfilterd[0];
  }

  function init() {
    wodList = options.wodList;
    initFilter();
  }

  function initFilter(){
    filterList = {
      name:".*",
      length: -1,
      amrap: true,
      ft: true,
      exercises:["ringmuscleups", "squatsnatches", "pullups", "pushups", "situps", "squats", "doubleunder", "handstandpushups", "ringdips", "muscleups", "kbswings", "burpees", "ringpushups", "frontsquat", "run", "climbers", "jumps", "lunges", "leglever"],
      equipment:["rings", "oly", "pullupbar", "jumprope", "muscleupbar", "kettlebell"]
    };
  }

  function addFilterValues(criteria, type, value) {
    switch (criteria){
      case "AMRAP":
        filterList.amrap = type;
        break;
      case "FT":
        filterList.ft = type;
        break;
      case "name":
        filterList.name = type;
        break;
      case "exercise":
        if (value){
          filterList.exercises.push(type);
        }
        else{
          filterList.exercises.splice(filterList.exercises.indexOf(type), 1);
        }
        break;
      case "equip":
        if (value){
          filterList.equipment.push(type);
        }
        else{
          filterList.equipment.splice(filterList.equipment.indexOf(type), 1);
        }
        break;
    }
  }

  function checkComponents(included, filter, type){
    var tmpIncludings = 0;
    filter.forEach(function(item){
      if(included.indexOf(item)>-1){
        tmpIncludings++;
      }
    });

    if(included.length === 0 || (tmpIncludings > 0 && ( type === 0 || (type === 2 && tmpIncludings === included.length)))){
      return true;
    }

    return false;
  }

  that.init = init;
  that.getWod = getWod;
  that.getWodList = getWodList;
  that.getFilteredWodList = getFilteredWodList;
  return that;
};

