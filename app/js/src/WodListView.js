/**
 * @namespace wod_list_view
 * @memberOf! mmp
 * @description View zur Darstellung der filterbaren Workoutliste
 * <p>Die <code>wod_list_view</code> verwaltet die filterbare Workoutliste. </p>
 * <p>Options-Objekt:</p>
 * <ul>
 * </ul>
 */
var App = App || {};
App.WodListView = function(options) {
  "use strict";
  /* eslint-env browser */

  var that = {},
    wodTemplate,
    wodListElement;

  function showWods(wodList){
    wodListElement.innerHTML = "";
    var data = wodList,
        i = 1;
    var entryNode = document.createElement("div");
    for(i = 0; i < data.length; i++) {
      entryNode.innerHTML = wodTemplate(data[i]);
      wodListElement.appendChild(entryNode.children[0]);
    }
  }

  function init() {
    createTamplate();
    wodListElement = options.wodListElement;
    showWods(options.wodList);
    setFilter();
  }

  function createTamplate(){
    wodTemplate = _.template(options.wodTemplate);
  }

  function emptyInputfield() {
    $("#submitWeightForm input").val("");
  }


  function showSuccessAlert(weight) {
    $("#submitWeightForm .alert-success").html("your weight off <strong>" + weight + " Kg </strong> was <strong>submitted!</strong>");
    $("#submitWeightForm .alert-success").addClass("in");
    setTimeout(function() {
      $("#submitWeightForm .alert-success").removeClass("in");
      $("#submitWeightForm .alert-success").empty();
    }, 3500);
  }

  function setFilter(){
    $(".chk-exercise, .chk-equip").prop("checked", true);
  }

  that.init = init;
  that.showSuccessAlert = showSuccessAlert;
  that.emptyInputfield = emptyInputfield;
  that.showWods = showWods;
  return that;
};

