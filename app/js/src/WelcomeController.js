var App = App || {};
App.WelcomeController = function() {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = new EventPublisher(),
    form,
    startButton,
    user;

  function getUserData() {
    var size, weight;
    form = document.getElementById("frmSetUserData");
    size = form.bodySize.value;
    weight = form.weight.value;
    size.replace(".", "");
    weight.replace(",", ".");
    user = {
      type: "user",
      name: form.name.value,
      size: size,
      sex: form.sex.options[sex.selectedIndex].value,
      birthdate: form.birthdate.value,
      weight: [{
        weight: weight,
        timestamp: Date.now()
      }],
      done: [],
    };

  }

  /**
   * tests if every inputfield was filled 
   * @return {bool}
   */
  function checkForm() {
    for (var property in user) {
      if (user.hasOwnProperty(property)) {
        if (user[property] === undefined || user[property] === "") {
          return false;
        }
      };
    };
    return true;
  }

  startButton = document.getElementById("startButton");
  startButton.addEventListener("click", function(event) {
    getUserData();
    that.notifyAll("onUserDataSubmited", user);
  });

  function getUser() {
    return user;
  }

  $('#frmSetUserData').submit(function(e) {
      e.preventDefault();
  });

  that.checkForm = checkForm;
  that.getUser = getUser;
  return that;
};