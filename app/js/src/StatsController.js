var App = App || {};
App.statsController = function() {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = new EventPublisher();

  
  function init() {
  	var listEntry = document.getElementById("stats-list");
    listEntry.addEventListener("click", function(event) {
      that.notifyAll("statsListEntryClicked", getParentWithClass(event.target, "stats_list_entry").getAttribute("wod-id"));
    });
  }

  /**
   * helper function to find the whole element that was clicked
   * @param  {[type]} element
   * @param  {string} targetClass
   * @return {none}
   */
  function getParentWithClass(element, targetClass) {
    if (element.classList.contains(targetClass)) {
      return element;
    } else if (element.parentElement !== null){
      return getParentWithClass(element.parentElement, targetClass);
    }
    return null;
  }

  that.init = init;
  return that;
};