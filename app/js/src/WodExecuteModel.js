var App = App || {};
App.wodExecuteModel = function() {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = new EventPublisher(),
    currentWod,
    user,
    timer;

  function init(user) {
    user = user;
  }

  /**
   * adds the new score to the done array an returns the modyfied object.
   * @param {object} user
   * @param {int} score number of reps
   */
  function addRepsToUser(currentUser, score) {
    var scores = currentUser.done[currentWod.id];
    if (timer.time === 0) {
      if (!scores) {
        scores = [];
      }
      var date = Date.now();
      scores.push({
        timestamp: date,
        score: score
      });
      currentUser.done[currentWod.id] = scores;
      that.notifyAll("onScoreSubmited", true);
      clearTimer();
    }
    return currentUser;
  }

  /**
   * adds the new score to the done array an returns the modyfied object.
   * @param {object} user
   * @param {int} score time needed in ms
   */
  function addTimeToUser(currentUser) {
    var scores = currentUser.done[currentWod.id];
    if (timer.time > 15000) {
      if (!scores) {
        scores = [];
      }
      var date = Date.now();
      var time = (timer.time/60000);
      
      scores.push({
        timestamp: date,
        score: time
      });
      currentUser.done[currentWod.id] = scores;
      that.notifyAll("onScoreSubmited", true);
      clearTimer();
    } else {
      that.notifyAll("onScoreSubmited", false);
    }
    return currentUser;
  }

  function initTimer(mode, amrapTime) {
    timer = {
      started: false,
      paused: false,
      start: 0,
      delayStart: 0,
      delay: 0,
      time: 0,
      timeReadable: [0,0,0,0],
      mode: mode,
      amrapTime: amrapTime,
      timer: null
    };
  }

  function startTimer() {
    timer.started = true;
    timer.start = Date.now();
    timer.timer = setInterval(run, 51);
    that.notifyAll("disableTimeSubmitButton", true);
  }

  function pauseTimer() {
    timer.paused = true;
    timer.delayStart = Date.now();
    clearInterval(timer.timer);
    that.notifyAll("disableTimeSubmitButton", false);
  }

  function resumeTimer() {
    timer.paused = false;
    timer.delay += Date.now() - timer.delayStart;
    timer.timer = setInterval(run, 51);
    that.notifyAll("disableTimeSubmitButton", true);
  }

  function run() {
    switch (timer.mode) {
      case "amrap":
        timer.time = timer.amrapTime - (Date.now() - timer.start - timer.delay);
        if (timer.time < 0) {
          clearInterval(timer.timer);
          timer.time = 0;
          that.notifyAll("disableTimeSubmitButton", null);
          that.notifyAll("showRepsSubmitButton", null);
        }
        break;
      case "ft":
        timer.time = (Date.now() - timer.start - timer.delay);
        break;
    }
    that.notifyAll("setTimerTime", parseTime(timer.time));
  }

  function parseTime(time){
    var factor = [3600000, 60000, 1000, 10];
    var i = 0;

    while (i < factor.length) {
      var t = Math.floor(time / factor[i]);
      time -= t * factor[i];
      t = (i > 0 && t < 10) ? '0' + t : t;
      timer.timeReadable[i] = t;
      i++;
    }
    return timer.timeReadable[0] + ":" + timer.timeReadable[1] + ":" + timer.timeReadable[2] + "." + timer.timeReadable[3];
  }

  function toggleTimer() {
    if (!timer.started) {
      startTimer();
      that.notifyAll("toogleTimerButton", "Pause");
    }
    else if (timer.paused) {
      resumeTimer();
      that.notifyAll("toogleTimerButton", "Pause");
    }
    else {
      pauseTimer();
      that.notifyAll("toogleTimerButton", "Resume");
    }
  }

  function clearTimer(){
    initTimer(currentWod.type, currentWod.length);
    currentWod.timeReadable = parseTime(currentWod.length);
    that.notifyAll("setView", currentWod);
  }

  function setCurrentWod(wod) {
    currentWod = wod;
    clearTimer();
  }

  that.user = user;
  that.setCurrentWod = setCurrentWod;
  that.toggleTimer = toggleTimer;
  that.addTimeToUser = addTimeToUser;
  that.addRepsToUser = addRepsToUser;
  return that;
}