var App = App || {};
App.viewController = function() {
  "use strict";
  /* eslint-env browser  */
  /*global EventPublisher */

  var that = new EventPublisher();

  function setEvents() {
    document.getElementById("navWodList").addEventListener("click", function(event) {
      that.notifyAll("onNavWodListClicked", event);
    });
    document.getElementById("navStats").addEventListener("click", function(event) {
      that.notifyAll("onNavStatsClicked", event);
    });
  }

  function showView(view) {
        $("#LoginScreen").addClass("hidden");
        $("#wodList").addClass("hidden");
        $("#wodExecute").addClass("hidden");
        $("#stats").addClass("hidden");
    switch(view) {
      case "login":
        $("#LoginScreen").removeClass("hidden");
        break;
      case "list":
        $("#wodList").removeClass("hidden");
        $(".btn-nav").removeClass("hidden");
        break;
      case "execute":
        $("#wodExecute").removeClass("hidden");
        break;
      case "stats":
        $("#stats").removeClass("hidden");
        break;
      default:
        $("#wodList").removeClass("hidden");
    }
  }

  function init() {
    setEvents();
  }

  that.init = init;
  that.showView = showView;
  return that;
};